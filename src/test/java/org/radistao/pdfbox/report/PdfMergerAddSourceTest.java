package org.radistao.pdfbox.report;

import org.apache.pdfbox.cos.COSDictionary;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.Collections;
import java.util.List;

import static org.radistao.PdfMergeResultTestUtils.assertDestinationPdf;
import static org.radistao.PdfMergeResultTestUtils.ensurePathExists;

public class PdfMergerAddSourceTest {

    @Rule
    public Timeout globalTimeout= Timeout.millis(5000);

    /**
     * Marker test: <i>SDE.pdf</i> is a good document and should always success.
     */
    @Test
    public void mergeGood_SDE() throws Exception {
        final String fileName = "good-files/SDE.pdf";
        final String destinationFileName = "testOutput/PdfMergerAddSourceTest/SDE.pdf";
        ensurePathExists(destinationFileName);

        PdfMergerAddSource pdfMergerAddSource = new PdfMergerAddSource();
        pdfMergerAddSource.mergeAddSource(Collections.singletonList(fileName), destinationFileName);

        assertDestinationPdf(destinationFileName);
    }

    /**
     * <i>certificate-of-analysis.pdf</i> fails with {@link StackOverflowError} falling to loop
     * in {@link org.apache.pdfbox.pdfwriter.COSWriter#visitFromDictionary(COSDictionary)}.
     *
     * @see PdfMergerAppendDocumentTest#mergeBad_CertificateOfAnalysis()
     */
    @Test
    public void mergeBad_CertificateOfAnalysis() throws Exception {
        final String fileName = "bad-files/certificate-of-analysis.pdf";
        final String destinationFileName = "testOutput/PdfMergerAddSourceTest/certificate-of-analysis.pdf";
        ensurePathExists(destinationFileName);

        PdfMergerAddSource pdfMergerAddSource = new PdfMergerAddSource();
        pdfMergerAddSource.mergeAddSource(Collections.singletonList(fileName), destinationFileName);

        assertDestinationPdf(destinationFileName);
    }

    /**
     * <i>questionario-sulle.pdf</i> falls to {@link java.io.IOException} with message:
     * <i>can't merge source document containing dynamic XFA form content.</i>.
     *
     * <b>Note:</b> <i>questionario-sulle.pdf</i> works fine in pdfbox version 1.8.13
     * using the same {@link PdfMergerAddSource#mergeAddSource(List, String)} workflow,
     * but also fails in {@link PdfMergerAppendDocumentTest#mergeBad_QuestionarioSulleUsingAppending()}
     *
     * To test v1.8.13 - update <i>build.gradle</i> file:
     * <pre>
     *     compile 'org.apache.pdfbox:pdfbox-app:1.8.13'
     * </pre>
     */
    @Test
    public void mergeBad_QuestionarioSulle() throws Exception {
        final String fileName = "bad-files/questionario-sulle.pdf";
        final String destinationFileName = "testOutput/PdfMergerAddSourceTest/questionario-sulle.pdf";
        ensurePathExists(destinationFileName);

        PdfMergerAddSource pdfMergerAddSource = new PdfMergerAddSource();
        pdfMergerAddSource.mergeAddSource(Collections.singletonList(fileName), destinationFileName);

        assertDestinationPdf(destinationFileName);
    }

}
# PDFBOX v2.0.4 bad files report

I have 2 files which fall pdfbox merge workflow into exceptions.
To provide you the tests I implemented 2 ways to merge PDF files:

  - `PdfMergerAddSource`:
    - create `PDFMergerUtility`
    - add sources using `PDFMergerUtility#addSource(InputStream)`
    - set destination file name `PDFMergerUtility#setDestinationFileName(String)`
    - merge using `PDFMergerUtility#mergeDocuments`

  - `PdfMergerAppendDocument`
    - create `PDFMergerUtility` and destination `PDDocument`
    - append sources using `PDFMergerUtility#appendDocument(PDDocument, PDDocument)`
    - save and close destination document: `PDDocument#save(File)` and `PDDocument#close()`

Bad files examples are provided in `test/resources`:

 - _certificate-of-analysis.pdf_
 - _questionario-sulle.pdf_

Also as a mock I've provided _SDE.pdf_ file, which always merged
successfully.

## How to run into errors

Run tests cases: `PdfMergerAddSourceTest` and
`PdfMergerAppendDocumentTest`. Every of them runs the tests
against those 3 test PDF files.

Build destination PDF files are stored in `/testOutput` directory,
ignored by VCS.

You may use full gradle build workflow:

```./gradlew build```

This will build 2 classes `PdfMergerAddSource` and
`PdfMergerAppendDocument` and run 6 tests (3 files against every class).

## Additional info

  - You may find useful Javadoc comments to the source and tests class
methods.
  - `questionario-sulle.pdf` is merged successfully in _pdfbox v1.8.13_
   using `PdfMergerAddSource/PdfMergerAddSourceTest`
  - following [Filing a Bug Report or Enhancement Request](http://pdfbox.apache.org/support.html)
  I tried to find any latest `SNAPSHOT` versions, but have not found.
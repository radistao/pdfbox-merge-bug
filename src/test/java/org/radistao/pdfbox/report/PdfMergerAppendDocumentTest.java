package org.radistao.pdfbox.report;

import org.apache.pdfbox.cos.COSDictionary;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.Collections;

import static org.radistao.PdfMergeResultTestUtils.assertDestinationPdf;
import static org.radistao.PdfMergeResultTestUtils.ensurePathExists;

public class PdfMergerAppendDocumentTest {

    @Rule
    public Timeout globalTimeout= Timeout.millis(5000);

    /**
     * Marker test: <i>SDE.pdf</i> is a good document and should always success.
     */
    @Test
    public void mergeGood_SDE() throws Exception {
        final String fileName = "good-files/SDE.pdf";
        final String destinationFileName = "testOutput/PdfMergerAppendDocumentTest/SDE.pdf";
        ensurePathExists(destinationFileName);

        PdfMergerAppendDocument pdfMergerAppendDocument = new PdfMergerAppendDocument();
        pdfMergerAppendDocument.mergeAppendDocument(Collections.singletonList(fileName), destinationFileName);

        assertDestinationPdf(destinationFileName);
    }

    /**
     * <i>certificate-of-analysis.pdf</i> falls by timeout using
     * {@link PdfMergerAppendDocument#mergeAppendDocument(java.util.List, java.lang.String)}
     *
     * @see PdfMergerAddSourceTest#mergeBad_CertificateOfAnalysis()
     */
    @Test(timeout = 5000L)
    public void mergeBad_CertificateOfAnalysis() throws Exception {
        final String fileName = "bad-files/certificate-of-analysis.pdf";
        final String destinationFileName = "testOutput/PdfMergerAppendDocumentTest/certificate-of-analysis.pdf";
        ensurePathExists(destinationFileName);

        PdfMergerAppendDocument pdfMergerAppendDocument = new PdfMergerAppendDocument();
        pdfMergerAppendDocument.mergeAppendDocument(Collections.singletonList(fileName), destinationFileName);

        assertDestinationPdf(destinationFileName);
    }

    /**
     * <i>questionario-sulle.pdf</i> falls to {@link java.io.IOException} with message:
     * <i>can't merge source document containing dynamic XFA form content.</i>.
     *
     * In pdfbox version 1.8.13 it fells to this error:
     * {@link org.apache.pdfbox.exceptions.COSVisitorException} caused by {@link NullPointerException}
     * in {@link org.apache.pdfbox.pdfwriter.COSWriter#visitFromDictionary(COSDictionary)}
     */
    @Test
    public void mergeBad_QuestionarioSulleUsingAppending() throws Exception {
        final String fileName = "bad-files/questionario-sulle.pdf";
        final String destinationFileName = "testOutput/PdfMergerAppendDocumentTest/questionario-sulle.pdf";
        ensurePathExists(destinationFileName);

        PdfMergerAppendDocument pdfMergerAppendDocument = new PdfMergerAppendDocument();
        pdfMergerAppendDocument.mergeAppendDocument(Collections.singletonList(fileName), destinationFileName);

        assertDestinationPdf(destinationFileName);
    }



}
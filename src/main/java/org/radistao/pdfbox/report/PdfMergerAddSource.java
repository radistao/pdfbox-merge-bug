package org.radistao.pdfbox.report;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

public class PdfMergerAddSource {

    /**
     * Merge files from {@code resourceNames} list to single PDF document.
     *
     * This solutions uses the next merge workflow:<ul>
     *     <li>create {@link PDFMergerUtility}</li>
     *     <li>add sources using {@link PDFMergerUtility#addSource(InputStream)}</li>
     *     <li>set destination file name {@link PDFMergerUtility#setDestinationFileName(String)}</li>
     *     <li>merge using {@link PDFMergerUtility#mergeDocuments()}</li>
     * </ul>
     *
     * @param resourceNames list of file names in <i>resources</i> directory
     * @param destinationFileName path to file where to same merge result
     */
    public void mergeAddSource(List<String> resourceNames, String destinationFileName)
            throws IOException {

        Objects.requireNonNull(resourceNames, "resourceNames must be non-null");
        Objects.requireNonNull(destinationFileName, "destinationFileName must be non-null");

        PDFMergerUtility ut = new PDFMergerUtility();

        final ClassLoader classLoader = getClass().getClassLoader();
        for (final String resourceName : resourceNames) {
            final InputStream resourceAsStream = classLoader.getResourceAsStream(resourceName);

            if (resourceAsStream == null) {
                throw new IllegalArgumentException(format("Resource [%s] is not available", resourceName));
            }

            ut.addSource(resourceAsStream);
        }

        ut.setDestinationFileName(destinationFileName);
        ut.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
    }
}

package org.radistao;

import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;

import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public final class PdfMergeResultTestUtils {

    public static void ensurePathExists(String path) {
        File pathFile = new File(path);
        if (!pathFile.exists()) {
            File parent = new File(pathFile.getParent());
            if (!parent.exists()) {
                if (!parent.mkdirs()) {
                    throw new IllegalArgumentException(
                            format("Can't create parent for [%s] path", path));
                }
            }
        }
    }

    /**
     * Assert that the file in {@code destinationFileName}:<ul>
     *     <li>exists</li>
     *     <li>has non null size</li>
     * </ul>
     *
     * @param destinationFileName file path to verify.
     */
    public static void assertDestinationPdf(String destinationFileName) throws Exception {
        final File mergedFile = new File(destinationFileName);

        assertThat(format("Destination file [%s] does not exist", destinationFileName),
                mergedFile.isFile(), is(true));
        assertThat(format("Destination file [%s] is empty", destinationFileName),
                mergedFile.exists(), is(true));

        try {
            PDDocument.load(mergedFile);
        } catch (Throwable e) {
            throw new AssertionError(format("Destination file [%s] can't be parsed. Reason: %s",
                    destinationFileName, e.getMessage()));
        }
    }
}

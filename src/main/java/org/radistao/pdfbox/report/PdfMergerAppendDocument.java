package org.radistao.pdfbox.report;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

public class PdfMergerAppendDocument {

    /**
     * Merge files from {@code resourceNames} list to single PDF document.
     *
     * This solutions uses the next merge workflow:<ul>
     *     <li>create {@link PDFMergerUtility} and destination {@link PDDocument}</li>
     *     <li>append sources using {@link PDFMergerUtility#appendDocument(PDDocument, PDDocument)}</li>
     *     <li>save and close destination document: {@link PDDocument#save(File)} and {@link PDDocument#close()}</li>
     * </ul>
     *
     * @param resourceNames list of file names in <i>resources</i> directory
     * @param destinationFileName path to file where to same merge result
     */
    public void mergeAppendDocument(List<String> resourceNames, String destinationFileName)
            throws IOException {

        Objects.requireNonNull(resourceNames, "resourceNames must be non-null");
        Objects.requireNonNull(destinationFileName, "destinationFileName must be non-null");

        PDFMergerUtility merger = new PDFMergerUtility();

        PDDocument dst = new PDDocument();

        final ClassLoader classLoader = getClass().getClassLoader();
        for (final String resourceName : resourceNames) {
            final InputStream resourceAsStream = classLoader.getResourceAsStream(resourceName);

            if (resourceAsStream == null) {
                throw new IllegalArgumentException(format("Resource [%s] is not available", resourceName));
            }

            PDDocument source = PDDocument.load(resourceAsStream);
            merger.appendDocument(dst, source);
            source.close();
        }

        dst.save(destinationFileName);
        dst.close();
    }
}
